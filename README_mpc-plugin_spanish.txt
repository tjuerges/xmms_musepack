--------------------- .mpc Musepack README -------------------------

As� que has decidido bajar el plugin Musepack para WinAMP.
Si tienes alguna sugerencia o si encuentras bugs en el plugin, por favor env�a un e-mail a:

    Andree.Buschmann@web.de


.mpc Musepack (tm)  es una marca registrada de Andree Buschmann

--------------------------------------------------------------------

INSTALACI�N
~~~~~~~~~~~~

XMMS

    Simplemente copia el archivo "xmms-musepack-*.dll" en el subdirectorio
    /usr/X11/lib/xmms/Input de tu directorio de XMMS y reinicia XMMS. Eso es
    todo. XMMS reconoce sus plugins autom�ticamente.


--------------------------------------------------------------------

Preguntas Frecuentes (FAQ)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Q1: �Los archivos MPC suenan m�s opacos que los archivos WAV originales al compararlos con Winamp!
A1: Por favor aseg�rate de haber desactivado las opciones "Clipping prevention" y "Replaygain"
    en el men� de configuraci�n del plugin. Esto cambia el volumen de la reproducci�n para evitar
    "clipping" y/o cambia el volumen de la pista para hacer las pistas igual de fuertes. Mucha gente
    percibe m�s fuerte como mejor.

Q2: Las tags ID3v2 no son reconocidas!
A2: El plugin s�lo ignorar� las tags. La reproducci�n y b�squeda dentro de archivos con tags ID3v2
    es posible.

Q3: La b�squeda en el archivo tarda mucho m�s que en pistas MP3!
A3: El plugin MPC tiene que buscar en el archivo cuadro por cuadro. Por lo tanto
    el tiempo de b�squeda depende del disco duro y de la potencia del CPU de tu sistema.
    system.

Q4: Para qu� sirve "Dithering"?
A4: La se�al de salida del decodificador tiene que ser cuantizada a resoluci�n de 16 bits.
    Normalmente esto se hace por truncamiento o redondeo de los valores de punto flotante a �ntegros.
    El redondeo al igual que el truncamiento lleva a distorciones arm�nicas. Para evitar estas
    distorsiones se agrega un ruido de baja energ�a a los valores de punto flotante antes de
    redondear al �ntegro de 16 bits. Este procedimiento se llama "Dithering".


--------------------------------------------------------------------
