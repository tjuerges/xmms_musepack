#ifndef _synth_filter_h_
#define _synth_filter_h_

/* D E F I N E S */
#define ftol(A,B) {tmp = *(int*) & A - 0x4B7F8000; B = (short)( (tmp==(short)tmp) ? tmp : (tmp>>31)^0x7FFF);}

/* V A R I A B L E N */
extern float Y_L[36][32];
extern float Y_R[36][32];

/* F U N K T I O N E N */
void Reset_V(void);
void Synthese_Filter_opt(short*);


#endif
