#include <math.h>
#include "requant.h"

/* V A R I A B L E N */
float orgSCF[64];               // tabellierten original-Skalenfaktoren
float SCF[64];                  // tabellierte Skalenfaktoren nach Clipping Prevention
unsigned int Q_bit[32];         // Anzahl bits fuer Speicherung der Aufloesung (SV6)
unsigned int Q_res[32][16];     // Index -> Aufloesung (SV6)
int Max_Band;

/* K O N S T A N T E N */
// bits per sample fuer gewaehlte Aufloesungsstufe
const unsigned int  Res_bit [18] = {
    0,  0,  0,  0,  0,  0,  0,  0,  7,
    8,  9, 10, 11, 12, 13, 14, 15, 16
};

// Requantisierungs-Koeffizienten
// 65536/step bzw. 65536/(2*D+1)
const float  __Cc [1 + 18] = {
  111.285962475327f,                                        // 32768/2/255*sqrt(3)
65536.000000000000f, 21845.333333333332f, 13107.200000000001f, 9362.285714285713f,
 7281.777777777777f,  4369.066666666666f,  2114.064516129032f, 1040.253968253968f,
  516.031496062992f,   257.003921568627f,   128.250489236790f,   64.062561094819f,
   32.015632633121f,    16.003907203907f,     8.000976681723f,    4.000244155527f,
    2.000061037018f,     1.000015259021f
};

// Requantisierungs-Offset
// 2*D+1 = steps of quantizer
const int  __Dc [1 + 18] = {
    2,
    0,     1,     2,     3,     4,     7,    15,    31,    63,
  127,   255,   511,  1023,  2047,  4095,  8191, 16383, 32767
};

/* F U N K T I O N E N */
void Skalenfaktoren(void)
{
    int n;
    //Abdeckung von +1.58...-98.41 dB, wobei scf[n]/scf[n-1] = 1.200508058
    for (n=0; n<64; ++n)
    {
        SCF[n] = orgSCF[n] = (float)( pow(10.,-0.1*(n-1)/1.26) );
    }
}

void ClipPrev ( const float fac )
{
    int n;

    // rescale the scalefactors
    for (n=0; n<64; ++n)
        SCF[n] = orgSCF[n] * fac;
}

void Quantisierungsmodes(void)
{
    int Band;
    //Zuordnung Index->Quantisierungsstufe (Bitstrom lesen)
    //Zuordnung Quantisierungsstufe->Index (Bitstrom schreiben)
    for (Band=0; Band<=10; ++Band)
    {
        Q_bit[Band]=4;
        Q_res[Band][0]=0;
        Q_res[Band][1]=1;
        Q_res[Band][2]=2;
        Q_res[Band][3]=3;
        Q_res[Band][4]=4;
        Q_res[Band][5]=5;
        Q_res[Band][6]=6;
        Q_res[Band][7]=7;
        Q_res[Band][8]=8;
        Q_res[Band][9]=9;
        Q_res[Band][10]=10;
        Q_res[Band][11]=11;
        Q_res[Band][12]=12;
        Q_res[Band][13]=13;
        Q_res[Band][14]=14;
        Q_res[Band][15]=17;
    }
    for (Band=11; Band<=22; ++Band)
    {
        Q_bit[Band]=3;
        Q_res[Band][0]=0;
        Q_res[Band][1]=1;
        Q_res[Band][2]=2;
        Q_res[Band][3]=3;
        Q_res[Band][4]=4;
        Q_res[Band][5]=5;
        Q_res[Band][6]=6;
        Q_res[Band][7]=17;
    }
    for (Band=23; Band<=31; ++Band)
    {
        Q_bit[Band]=2;
        Q_res[Band][0]=0;
        Q_res[Band][1]=1;
        Q_res[Band][2]=2;
        Q_res[Band][3]=17;
    }
}

void initialisiere_Quantisierungstabellen(void)
{
    Quantisierungsmodes();
    Skalenfaktoren();
}
