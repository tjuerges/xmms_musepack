----------------------.mpc Musepack-README -------------------------

Moin, moin ;-)

 Du hast Dich also dazu entschlossen das Musepack-Plugin zu down-
 loaden (oder downzuloaden?!). Falls irgendwelche Verbesserungs-
 vorschl�ge anfallen oder noch irgendwelche �blen Bugs auftreten
 sollten, w�rde ich mich �ber eine Mail freuen.

    Andree.Buschmann@web.de


.mpc Musepack ist ein eingetragener Markenname von Andree Buschmann

--------------------------------------------------------------------

INSTALLATION
~~~~~~~~~~~~

XMMS

    Einfach die Datei "xmms-musepack-*.dll" in das Verzeichnis
    "/usr/X11/lib/xmms/Input" der XMMS-Installation kopieren und XMMS
    neustarten. XMMS erkennt seine Plugins selbst�ndig.


--------------------------------------------------------------------

Frequently Ask Questions (FAQ)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Q1: MPC Dateien klingen dumpfer als die Original-WAV Dateien, wenn diese
    �ber WinAMP abgespielt werden.
A1: Bitte die Option "Clippingschutz" in der Konfiguration des MPC Plugins
    deaktivieren.

Q2: ID3v2-Tags werden nicht ausgewertet.
A2: Ja. ID3v2-Tags werden vom Plugin nicht ausgewertet, sondern nur
    ignoriert, so da� Wiedergabe und Spulen funktioneren.

Q3: Das Anspringen einer bestimmente Zeitmarke dauert MPC l�nger als bei
    z.B. MP3.
A3: Das MPC Plugin mu� die gesamte Frame f�r Frame durchlaufen. Daher h�ngt
    die Suchdauer von der Performance der Festplatte und der CPU-Leistung
    ab. Es sind allerdings einige Verbesserungen in letzter Zeit
    eingeflossen, so da� dieses Problem nur bei langsamen Computer und
    langen Musikst�cken auffallen sollte (z.B. Pentium-133 und 22:37
    Spiell�nge).

Q4: Was ist "Dither"?
A4: Das Ausgangssignal des Decoders mu� f�r die Wiedergabe auf 16 bit
    quantisiert werden. Normalerweise geschieht dies durch einfaches
    Abschneiden der LSBs oder durch Runden. Beide Methoden f�hren zu
    aharmonischen Verzerrungen (Intermodulation). Um diese zu vermeiden, wird vor
    der Quantisierung ein Rauschsignal geringer Leistung zum
    Ausgangssignal addiert. Dies wird als "Dithering" bezeichnet.

Q5: Es knackt zwischen den Titeln.
A5: Derzeitig hilft nur:
    - Equalizer ausschalten
    - keine titelbasierten, sondern nur albumbasierte Replaygains +
      ClippingPreventions verwenden

--------------------------------------------------------------------
