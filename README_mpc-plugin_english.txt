--------------------- .mpc Musepack README -------------------------

So, you've decided to download the Musepack plugin for WinAMP.
If you have any suggestions or you find any bugs inside the plugin,
feel so free to send a e-mail to

    Andree.Buschmann@web.de


.mpc Musepack (tm)  is a registered trademark of Andree Buschmann

--------------------------------------------------------------------

INSTALLATION
~~~~~~~~~~~~

XMMS

    Simply copy the file "xmms-musepack-*.dll" in the /usr/X11/lib/xmms/Input
    subdirectory and restart XMMS. This should be all. XMMS recognizes it's
    plugins automatically.


--------------------------------------------------------------------

Frequently Ask Questions (FAQ)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Q1: MPC files sound duller than the original WAVE files when comparing via
    WinAMP!
A1: Please make sure you disabled "Clipping prevention" and "Replaygain"
    feature in the plugin configuration menu. This changes the loudness of
    the playback to avoid clipping and/or changes the titles loudness to
    make the titles more equal loud. Many people feel louder as better.

Q2: ID3v2 tags are not recognized!
A2: The plugin will only ignore these tags. Playback of and seek within
    ID3v2 tagged files is possible.

Q3: The file seek takes much more time than seeking in MP3 tracks!
A3: The MPC plugin has to scan the entire file frame by frame. Therefore
    the seeking time depends on the hard disk and the CPU power of your
    system.

Q4: What does Dithering do?
A4: The output signal of the decoder has to be quantized to 16 bit
    resolution. Normally this is done either by truncation or rounding of
    the floating point values to integer. The rounding as well as the
    truncation leads to harmonic distortions. To prevent from these
    distortions there is a low-energy noise added to the floating point
    values before rounding to the 16 bit integer. This procedure is called
    "Dithering".

--------------------------------------------------------------------
