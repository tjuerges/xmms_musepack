#ifndef _mpc_dec_h_
#define _mpc_dec_h_

/* D E F I N E S */
#define EQ_TAP 13           // length of FIR filter for EQ
#define DELAY (EQ_TAP+1)/2  // delay of FIR
#define FIR_BANDS 4         // number of subbands to be FIR-filtered

/* F U N K T I O N S */
int DECODE(char*,int*);
void RESET_Synthesis(void);
void RESET_Globals(void);
void Lese_Bitstrom_SV6(void);
void Lese_Bitstrom_SV7(void);

/* V A R I A B L E S */
// globale Variablen fuer EQ
extern float EQ_gain[32-FIR_BANDS];
extern float EQ_Filter[FIR_BANDS][EQ_TAP];
extern unsigned int EQ_activated;

// globale Variablen fuer ClippingPrevention
extern float ClipPrevFactor;
extern unsigned int ClipPrev_activated;

// weitere globale Flags
extern unsigned int StreamVersion;
extern unsigned int MS_used;        //globaler Flag fuer M/S-Signalfuehrung
extern unsigned int FwdJumpInfo;
extern unsigned int ActDecodePos;
extern unsigned int OverallFrames;
extern unsigned int DecodedFrames;

#endif
