VERSION = 0.94

ifdef ARCH
else
ARCH=-march=athlon -mcpu=athlon
endif

SOURCE = in_mpc.c bitstream.c huff_new.c huff_old.c requant.c synth_filter.c mpc_dec.c

all:    xmms-musepack-$(VERSION).so

install-test: all
	ln -f -s `pwd`/xmms-musepack-`echo $(VERSION)`.so \
	~/.xmms/Plugins/xmms-musepack-`echo $(VERSION)`.so

install: all
	install -g root -o root -m 755 \
	./xmms-musepack-`echo $(VERSION)`.so \
	`xmms-config --input-plugin-dir`/xmms-musepack-`echo $(VERSION)`.so

xmms-musepack-$(VERSION).so: $(SOURCE) Makefile
	gcc -s -DVERSION=\"$(VERSION)\" $(ARCH) -Wall -pedantic -shared -O3 $(ARCH) --fast-math -DPOSIX -D_REENTRANT -DREENTRANT `xmms-config --cflags --libs` `gtk-config --cflags --libs` -o xmms-musepack-`echo $(VERSION)`.so $(SOURCE)

clean:
	rm -f *.o xmms-musepack-`echo $(VERSION)`.so

uninstall:
	rm -f `xmms-config --input-plugin-dir`/xmms-musepack-`echo $(VERSION)`.so
