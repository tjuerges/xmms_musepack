#ifndef _bitstream_decode_h_
#define _bitstream_decode_h_

typedef struct Huffmancode {
    unsigned int  Code;
    unsigned int  Length;
    int           Value;
} HuffmanTyp;

#define MEMSIZE  8192         // Buffer size in 32 bit units of both butterfly buffers together, must be power of 2
#define MEMSIZE2 (MEMSIZE/2)   // Buffer size in 32 bit units of one butterfly buffers
#define MEMMASK  (MEMSIZE-1)   // Mask for wrap around at the end of the buffer

/* V A R I A B L E N */
extern unsigned int Speicher[MEMSIZE];  // enthaelt den Lese-Puffer
extern unsigned int dword;              // 32Bit-Wort fuer Bitstrom-I/O
extern unsigned int pos;                // Position im aktuell decodierten 32Bit-Wort
extern unsigned int Zaehler;            // aktuelle Position im Lese-Puffer

/* P R O Z E D U R E N */
void          Reset_BitstreamDecode(void);
unsigned int  BitsRead(void);
unsigned int  Bitstream_read(const unsigned int);
int           Huffman_Decode       (const HuffmanTyp*);   // works with maximum lengths up to 14
int           Huffman_Decode_fast  (const HuffmanTyp*);   // works with maximum lengths up to 10
int           Huffman_Decode_faster(const HuffmanTyp*);   // works with maximum lengths up to  5
void          SCFI_Bundle_read(const HuffmanTyp*, int*, int*);
void          Resort_HuffTables     ( const unsigned int elements, HuffmanTyp* Table, const int offset );

#endif
