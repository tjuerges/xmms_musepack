#ifndef _requant_h
#define _requant_h_

/* V A R I A B L E N */
extern float SCF[64];                   //tabellierte Skalenfaktoren
extern unsigned int Q_bit[32];          //Anzahl bits fuer Speicherung der Aufloesung (SV6)
extern unsigned int Q_res[32][16];      //Index -> Aufloesung (SV6)
extern int Max_Band;

/* K O N S T A N T E N */
extern const unsigned int Res_bit[18];  // bits per sample fuer gewaehlte Aufloesungsstufe
extern const float __Cc[1+18];               //Requantisierungs-Koeffizienten
extern const int __Dc[1+18];                 //Requantisierungs-Offset


#define Cc (__Cc + 1)
#define Dc (__Dc + 1)

/* P R O Z E D U R E N */
void initialisiere_Quantisierungstabellen(void);
void ClipPrev(const float fac);

#endif
