#include "bitstream.h"

/* K O N S T A N T E N */
unsigned int  mask [33] = {
    0u,1u,3u,7u,15u,31u,63u,127u,
    255u,511u,1023u,2047u,4095u,8191u,16383u,32767u,
    65535u,131071u,262143u,524287u,1048575u,2097151u,4194303u,8388607u,
    16777215u,33554431u,67108863u,134217727u,268435455u,536870911u,1073741823u,2147483647u,4294967295u
};

/* V A R I A B L E N */
unsigned int Speicher[MEMSIZE];     // enthaelt den Lese-Puffer
unsigned int dword            = 0;  // 32Bit-Wort fuer Bitstrom-I/O
unsigned int pos              = 0;  // Position im aktuell decodierten 32Bit-Wort
unsigned int Zaehler          = 0;  // aktuelle Position im Lese-Puffer
static unsigned int WordsRead = 0;  // Zaehler fuer Anzahl decodierter 32Bit-Worte


/* F U N K T I O N E N */
// resets bitstream decoding
void Reset_BitstreamDecode(void)
{
    dword     = 0;
    pos       = 0;
    Zaehler   = 0;
    WordsRead = 0;
}

// reports the number of read bits
unsigned int BitsRead(void)
{
    return 32*WordsRead + pos;
}

// read desired number of bits out of the bitstream
unsigned int Bitstream_read(const unsigned int bits)
{
    unsigned int out = dword;

    pos += bits;

    if (pos<32)
    {
        out >>= (32-pos);
    }
    else
    {
        dword = Speicher[Zaehler=(++Zaehler)&MEMMASK];
        pos -= 32;
        if (pos)
        {
            out <<= pos;
            out |= dword >> (32-pos);
        }
        ++WordsRead;
    }

    return out & mask[bits];
}

// decode huffman
int Huffman_Decode(const HuffmanTyp *Table)
{
    // load preview and decode
    unsigned int code  = dword << pos;
    if (pos>18)  code |= Speicher[(Zaehler+1)&MEMMASK] >> (32-pos);
    while (code < Table->Code) Table++;

    // Setze Bitstromposition ohne dummy-read
    if ((pos += Table->Length)>=32)
    {
        pos -= 32;
        dword = Speicher[Zaehler=(++Zaehler)&MEMMASK];
        ++WordsRead;
    }

    return Table->Value;
}

// faster huffman through previewing less bits
int Huffman_Decode_fast(const HuffmanTyp *Table)
{
    // load preview and decode
    unsigned int code  = dword << pos;
    if (pos>22)  code |= Speicher[(Zaehler+1)&MEMMASK] >> (32-pos);
    while (code < Table->Code) Table++;

    // Setze Bitstromposition ohne dummy-read
    if ((pos += Table->Length)>=32)
    {
        pos -= 32;
        dword = Speicher[Zaehler=(++Zaehler)&MEMMASK];
        ++WordsRead;
    }

    return Table->Value;
}

// even faster huffman through previewing even less bits
int Huffman_Decode_faster(const HuffmanTyp *Table)
{
    // load preview and decode
    unsigned int code  = dword << pos;
    if (pos>27)  code |= Speicher[(Zaehler+1)&MEMMASK] >> (32-pos);
    while (code < Table->Code) Table++;

    // Setze Bitstromposition ohne dummy-read
    if ((pos += Table->Length)>=32)
    {
        pos -= 32;
        dword = Speicher[Zaehler=(++Zaehler)&MEMMASK];
        ++WordsRead;
    }

    return Table->Value;
}

// decode SCFI-bundle (sv4,5,6)
void SCFI_Bundle_read(const HuffmanTyp *Table, int *SCFI, int *DSCF)
{
    // load preview and decode
    unsigned int code  = dword << pos;
    if (pos>26)  code |= Speicher[(Zaehler+1)&MEMMASK] >> (32-pos);
    while (code < Table->Code) Table++;

    // Setze Bitstromposition ohne dummy-read
    if ((pos += Table->Length)>=32)
    {
        pos -= 32;
        dword = Speicher[Zaehler=(++Zaehler)&MEMMASK];
        ++WordsRead;
    }

    *SCFI = Table->Value>>1;
    *DSCF = Table->Value &1;
}


static int
cmpfn ( const void* p1, const void* p2 )
{
    if ( ((const HuffmanTyp*) p1)->Code < ((const HuffmanTyp*) p2)->Code ) return +1;
    if ( ((const HuffmanTyp*) p1)->Code > ((const HuffmanTyp*) p2)->Code ) return -1;
    return 0;
}


// sort huffman-tables by codeword
// offset resulting value
void
Resort_HuffTables ( const unsigned int elements, HuffmanTyp* Table, const int offset )
{
    unsigned int  i;

    for ( i = 0; i < elements; i++ ) {
        Table[i].Code <<= 32 - Table[i].Length;
        Table[i].Value  =  i - offset;
    }
    qsort ( Table, elements, sizeof(*Table), cmpfn );
}
